# Kookbook

#### Informacje pomocnicze

* Uruchomienie serwera: `yarn server-start`
* Zbudowanie klienta: `yarn client-build`
* Uruchomienie serwera i klienta do celów rozwoju: `yarn start`
* Uruchomienie wersji *dev* klienta: `yarn client-start`


#### Zadanie uzupełniające dla kandydatów na stanowisko "Programista React"

* Dodaj funkcjonalność wyszukania przepisu wg składników
* Dodaj do UI obsługę aplikacji z klawiatury
* Popraw estetykę i atrakcyjność wizualną aplikacji

