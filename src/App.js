import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import { Home, ErrorPage, Recipe } from "./pages";
import { Nav, Container, Footer } from "./comps";

function App() {
  return (
    <div>
      <Router>
        <Nav />
        <Container>
          <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/recipe/:id" component={Recipe} />
            <Route component={ErrorPage} />
          </Switch>
        </Container>
        <Footer />
      </Router>
    </div>
  );
}

export default App;
