import axios from "axios";
/* Recipies gathered from www.przepisy.pl */

const HOST = process.env.REACT_APP_HOST || window.location.hostname;
const PORT = process.env.REACT_APP_PORT || 3002;
const BASE_URL = `http://${HOST}:${PORT}`;

export const fetchPosts = () =>
  new Promise((resolve, reject) => {
    const url = `${BASE_URL}/posts`;

    axios
      .get(url)
      .then(res => {
        switch (res.status) {
          case 200:
            resolve(res.data);
            break;
          default:
            reject("An error occurred while fetching data.");
        }
        resolve(res);
      })
      .catch(err => {
        reject(err);
        console.error(err);
      });
  });

export const fetchPost = id =>
  new Promise((resolve, reject) => {
    const url = `${BASE_URL}/posts/${id}`;

    axios
      .get(url)
      .then(res => {
        resolve(res);
      })
      .catch(err => {
        reject(err);
        console.error(err);
      });
  });
