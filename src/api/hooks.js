import { useState, useEffect } from "react";
import { fetchPosts, fetchPost } from "./client";

export const useRecipes = (total = 10) => {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    fetchPosts().then(res => {
      setData([...res].slice(0, total));
      setLoading(false);
    });
  }, [total]);

  return [[...data], loading, setData];
};

export const useRecipe = id => {
  const [data, setData] = useState(null);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    fetchPost(id).then(res => {
      setData(res.data);
      setLoading(false);
    });
  }, [id]);

  return [data, loading, setData];
};
