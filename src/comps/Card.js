import React from "react";
import { withRouter } from "react-router-dom";
import PropTypes from "prop-types";

import cx from "classname";
import FlexWrapper from "../utils/FlexWrapper";

const Card = ({
  id,
  title,
  kind,
  time,
  thumbnail,
  level,
  ingredients,
  steps,
  featured,
  ...props
}) => {
  const handleClick = () => props.history.push(`/recipe/${id}`);

  return (
    <FlexWrapper
      justifyContent="flex-start"
      className={cx("Card", { featured })}
      onClick={handleClick}
    >
      <div className="Card__thumbnail">
        <img src={thumbnail} alt={title} />
      </div>
      <div className="Card__content">
        <h2 className="Card__title">{title}</h2>
        <p className="Card__kind">{kind}</p>
        <p>
          <b>Czas przygotowania</b> {time} minut
        </p>
        <p>
          <b>Poziom trudności</b> {levelToText(level)}
        </p>
        <p>{ingredients.length} składników</p>
        <p>{steps.length} kroków</p>
      </div>
    </FlexWrapper>
  );
};

const levelToText = level => {
  switch (level) {
    case 1:
    default:
      return "Łatwy";
    case 2:
      return "Średni";
    case 3:
      return "Trudny";
    case 4:
      return "Piekielny";
  }
};

Card.propTypes = {
  title: PropTypes.string.isRequired,
  time: PropTypes.number,
  thumbnail: PropTypes.string,
  level: PropTypes.number,
  ingredients: PropTypes.arrayOf(PropTypes.object),
  steps: PropTypes.arrayOf(PropTypes.string),

  featured: PropTypes.bool

  // + withRouter props: https://github.com/ReactTraining/react-router/blob/master/packages/react-router/docs/api/withRouter.md
};

Card.defaultProps = {
  featured: false,
  thumbnail:
    "https://i1.wp.com/awsprojects.net/wp-content/plugins/penci-pennews-portfolio/images/no-thumbnail.jpg"
};

export default withRouter(Card);
