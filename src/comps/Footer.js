import React from "react";

const Footer = () => {
  return (
    <div className="Footer">
      <div className="Footer__content">
        <p>Zdjęcia pochodzą z <a href="https://unsplash.com/">unsplash.com</a></p>
      </div>
    </div>
  );
};

export default Footer;
