import React from "react";
import { Link } from "react-router-dom";
import FlexWrapper from "../utils/FlexWrapper";

const Nav = () => {
  return (
    <div className="Nav">
      <FlexWrapper className="Nav__content">
        <Link to="/" className="Nav__logo">
          Kookbook
        </Link>
        <div className="Nav__actions">
          <a href="https://sokrates.pl/kontakt/">Kontakt</a>
        </div>
      </FlexWrapper>
    </div>
  );
};

export default Nav;
