import React from "react";
import { Card } from "../comps/";
import FlexWrapper from "../utils/FlexWrapper";
import { useRecipes } from "../api/hooks";

const ENTRIES_PER_PAGE = 10;

const Home = () => {
  const [recipes, loading] = useRecipes(ENTRIES_PER_PAGE);

  if (loading) return <h4>Ładowanie przepisów...</h4>;

  // In case there is no connection to the server.
  if (recipes.length === 0) return <h4>Brak przepisów do wyświetlenia.</h4>;

  return (
    <FlexWrapper flexWrap="wrap" justifyContent="flex-start" className="Home">
      {recipes.map((e, i) => (
        <Card key={e.id} {...e} featured={Boolean(i === 0)} />
      ))}
    </FlexWrapper>
  );
};

export default Home;
