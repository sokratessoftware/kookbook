import React from "react";
import { withRouter } from "react-router-dom";
import { useRecipe } from "../api/hooks";
import FlexWrapper from "../utils/FlexWrapper";

const Recipe = ({ match, history }) => {
  const recipe_id = match.params.id;
  const [recipe, loading] = useRecipe(recipe_id);

  if (!recipe_id) return <h3>Nie znaleziono przepisu..</h3>;
  if (loading) return <h3>Ładowane przepisu...</h3>;

  return (
    <div className="Recipe">
      <div className="Recipe__thumbnail">
        <img src={recipe.thumbnail} alt={recipe.title} />
      </div>
      <h1 className="Recipe__title">{recipe.title}</h1>
      <p className="Recipe__kind">{recipe.kind}</p>
      <br />
      <br />
      <FlexWrapper>
        <div className="Recipe__steps">
          {recipe.steps.map((e, i) => (
            <Step key={`step_${JSON.stringify(e)}`} value={e} index={i} />
          ))}
        </div>
        <div className="Recipe__ingredients">
          {recipe.ingredients.map(e => (
            <Ingredient key={`ingredient_${JSON.stringify(e)}`} {...e} />
          ))}
        </div>
      </FlexWrapper>
    </div>
  );
};

const Step = ({ value, index }) => (
  <div className="Recipe__step">
    <h3>Krok {index + 1}</h3>
    <p>{value}</p>
  </div>
);

const Ingredient = ({ name, amount }) => (
  <FlexWrapper className="Recipe__ingredient">
    <p>{name}</p>
    <p>{amount}</p>
  </FlexWrapper>
);

export default withRouter(Recipe);
