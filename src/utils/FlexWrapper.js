import PropTypes from "prop-types";
import React from "react";

const styleProps = {
  alignContent: PropTypes.oneOf([
    "flex-start",
    "flex-end",
    "center",
    "space-between",
    "space-around",
    "stretch"
  ]),
  alignItems: PropTypes.oneOf([
    "strech",
    "flex-start",
    "flex-end",
    "center",
    "baseline"
  ]),
  columnOnMobile: PropTypes.bool,
  flexDirection: PropTypes.oneOf([
    "row",
    "row-reverse",
    "column",
    "column-reverse"
  ]),
  flexFlow: PropTypes.string,
  flexWrap: PropTypes.oneOf(["nowrap", "wrap", "wrap-reverse"]),
  height: PropTypes.string,
  inline: PropTypes.bool,
  justifyContent: PropTypes.oneOf([
    "flex-start",
    "flex-end",
    "center",
    "space-between",
    "space-around",
    "space-evenly" // Not supported in Edge 18
  ]),
  width: PropTypes.string
};

const propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]).isRequired,
  style: PropTypes.object,
  ...styleProps
};

function FlexWrapper(props) {
  return (
    <div style={composeStyle()} {...filterProps()}>
      {props.children}
    </div>
  );

  function composeStyle() {
    let styles = {
      ...(props.style || {}),
      display: props.inline ? "inline-flex" : "flex"
    }; // Initial style.

    const _styleProps = Object.keys(styleProps || {});

    // Adds props to style.
    for (let i = 0; i < _styleProps.length; i++)
      if (props[_styleProps[i]]) styles[_styleProps[i]] = props[_styleProps[i]];

    if (props.columnOnMobile) styles.flexDirection = "column";

    return styles;
  }

  function filterProps() {
    const _propTypes = Object.keys(propTypes || {}),
      props2 = { ...props };

    // Delete all props defined in PropTypes.
    for (let i = 0; i < _propTypes.length; i++) delete props2[_propTypes[i]];

    return props2;
  }
}

FlexWrapper.styleProps = { ...styleProps };
FlexWrapper.propTypes = { ...propTypes };

FlexWrapper.defaultProps = {
  inline: false,
  justifyContent: "space-between"
};

export default FlexWrapper;
